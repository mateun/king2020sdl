#include <SDL.h>
#include <SDL_image.h>
#include <map>
#include <string>
#include <vector>

using namespace std;

// Loads all images to the GPU for later use during rendering
map<string, SDL_Texture*>loadImages(vector<string> imageIds, vector<string> imagePaths, SDL_Renderer* renderer) {
    map<string, SDL_Texture*> m;
    for (int i = 0; i < imageIds.size(); i++) {
        SDL_Surface* image = IMG_Load(imagePaths[i].c_str());
        SDL_Texture* tex = SDL_CreateTextureFromSurface(renderer, image);
        SDL_FreeSurface(image);
        if (tex == nullptr) {
            SDL_Log("error loading image, %s", imagePaths[i].c_str());
            exit(1);
        }
        m[imageIds[i]] = tex;
    }
    return m;
}

struct SDL_WinRenderer {
    SDL_Window* window;
    SDL_Renderer* renderer;
};


void init(int screenw, int screenh, SDL_Renderer** outRenderer, SDL_Window** outWindow) {
    
    // Clarify what this is really for...
    //SDL_Surface* screenSurface = NULL;
    
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		SDL_Log("SDL could not initialize: %s\n", SDL_GetError());
        exit(1);
	} else {
        
        // Needed for OpenGL
        //        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
        //        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
        
        //Create window
        *outWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED,
                                  SDL_WINDOWPOS_UNDEFINED, screenw, screenh,
                                  SDL_WINDOW_SHOWN);
        
        
        if( *outWindow == NULL ) {
            SDL_Log( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
        } else {
            
            int flags=IMG_INIT_JPG|IMG_INIT_PNG;
            int initted= IMG_Init(flags);
            
            if((initted&flags)  != flags) {
                SDL_Log("IMG_Init: Failed to init required jpg and png support!\n");
                SDL_Log("IMG_Init: %s\n", IMG_GetError());
                exit(1);
            }
            
            // Create a hardware accelerated renderer
            *outRenderer = SDL_CreateRenderer(*outWindow, -1,
                                                   SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if (*outRenderer == NULL){
                SDL_Log("SDL_CreateRenderer Error: %s\n" , SDL_GetError());
                exit(1);
            }

        }
        
    }
    
}

struct GameObject {
    float positionX;
    float positionY;
    string imageName;
    bool isNpc;
    bool isStatic;
};

struct Camera {
    float posX;
    float posY;
    int w;
    int h;
};

void update(float delta, vector<GameObject*> gameObjects, Camera& camera) {
    for (auto go : gameObjects) {
        if (!go->isNpc) {
            // handle player input
        }
        else {
            // just move the guy slowly downwards...
            go->positionY += 0;
        }
    }

    // Move the camera a bit:
    camera.posX += 0.1f;
    camera.posY -= 0.5f;
}

void render(SDL_Renderer* renderer, vector<GameObject*> gameObjects, map<string, SDL_Texture*> &imageMap, const Camera& camera) {
    for (auto go : gameObjects) {
        SDL_Rect targetRect;
        targetRect.x = go->positionX - camera.posX;
        targetRect.y = go->positionY - camera.posY;
        targetRect.w = 64;
        targetRect.h = 64;
        SDL_RenderCopy(renderer, imageMap[go->imageName], nullptr, &targetRect);
    }
}





int main(int argc, char* args[])
{
    SDL_Window* window;
    SDL_Renderer* renderer;
    init(800, 600, &renderer, &window);

    vector<string> ids;
    ids.push_back("hero");
    ids.push_back("plant");
    vector<string> paths;
    paths.push_back("E:/Projects/C++/SDLTest1/images/hero.png");
    paths.push_back("E:/Projects/C++/SDLTest1/images/plant.png");

    map<string, SDL_Texture*> imageMap = loadImages(ids, paths, renderer);

    vector<GameObject*> gameObjects;
    GameObject hero = { 360, 500, "hero", false, false };
    gameObjects.push_back(&hero);
    
    
    for (int c = 0; c < 10; c++) {
        GameObject* p = new GameObject { c * 70.0f, c * 10.0f, "plant", true, false };
        gameObjects.push_back(p);
    }
    
    Camera camera = { 100, 400, 800, 600 };

    SDL_Event e;
    bool quit = false;
    while (!quit){
        while (SDL_PollEvent(&e)){
            if (e.type == SDL_QUIT)
                quit = true;
            if (e.type == SDL_KEYDOWN) {
                //moveCameraByKeyboard(e.key.keysym.sym);
            }
            if (e.type == SDL_MOUSEBUTTONDOWN)
                quit = true;
        }

        update(16, gameObjects, camera);
        
        //Render the scene
        SDL_SetRenderDrawColor(renderer, 250, 100, 100, 255);
        SDL_RenderClear(renderer);
        render(renderer, gameObjects, imageMap, camera);
        SDL_RenderPresent(renderer);

    }
  
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow( window );
    SDL_Quit();
    
    return 0;
	
}
